<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teachers extends Model
{

    protected $table = 'teachers';

    public function faculty_teacher()
    {
        return $this->belongsTo('App\Facultys', 'faculty_id','id');
    }

    public function subject_teacher()
    {
        return $this->hasMany('App\Subjects','teacher_id','id');
    }
}
