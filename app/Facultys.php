<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facultys extends Model
{

    protected $table = 'facultys';

    public function class_students_faculty()
    {
        return $this->hasMany('App\ClassStudents','faculty_id','id');
    }

    public function teacher_facultys()
    {
        return $this->hasMany('App\Teachers', 'faculty_id', 'id');
    }
}
