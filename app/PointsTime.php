<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PointsTime extends Model
{

    protected $table = 'points_time';

    public function pointtype_timepoint()
    {
        return $this->belongsTo('App\PointsType','point_type_id','id');
    }

    public function student_pointtime()
    {
        return $this->hasMany('App\Students','point_time_id','id');
    }
}
