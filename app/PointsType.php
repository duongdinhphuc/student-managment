<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PointsType extends Model
{

    protected $table = 'point_type';

    public function pointtime_pointtype()
    {
        return $this->hasMany('App\PointsTime', 'point_type_id', 'id');
    }
}
