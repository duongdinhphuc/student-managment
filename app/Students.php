<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Students extends Model
{

    protected $table = 'students';

    public function class_st_student()
    {
        return $this->belongsTo('App\ClassStudents','class_student_id','id');
    }

    public function pointdetail_student()
    {
        return $this->hasMany('App\PointsDetail','student_id','id');
    }

    public function course_student()
    {
        return $this->belongsTo('App\Courses','course_id','id');
    }

    public function subject_student()
    {
        return $this->belongsTo('App\Subjects','subject_id','id');
    }

    public function pointtime_student()
    {
        return $this->belongsTo('App\PointsTime','point_time_id','id');
    }

}
