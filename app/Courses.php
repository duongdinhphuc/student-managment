<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Courses extends Model
{
    //
    protected $table = 'courses';

    public function student_course()
    {
        return $this->hasMany('App\Students','course_id','id');
    }
}
