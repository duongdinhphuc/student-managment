<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassStudents extends Model
{

    protected $table = 'class_students';

    public function facultys_class_students()
    {
        return $this->belongsTo('App\Facultys', 'faculty_id', 'id');
    }

    public function student_class_st()
    {
        return $this->hasMany('App\Students', 'class_student_id', 'id');
    }
}
