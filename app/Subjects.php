<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subjects extends Model
{

    protected $table = 'subjects';

    public function teacher_subject()
    {
        return $this->belongsTo('App\Teachers', 'teacher_id','id');
    }

    public function student_subject()
    {
        return $this->hasMany('App\Students','subject_id','id');
    }
}
