<?php

namespace App\Admin\Controllers;

use App\ClassStudents;
use App\Courses;
use App\PointsTime;
use App\Students;
use App\Http\Controllers\Controller;
use App\Subjects;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Support\Facades\DB;

class StudentsAdminController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form_edit()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Students);

        $grid->id('ID')->sortable();
        $grid->student_code('Student code');
        $grid->full_name('Full name')->editable();
//        $grid->column('alias','Alias')->display(function (){
//            return str_slug($this->full_name);
//        });
//        $grid->gender('Gender')->editable('select', [
//            "m" => "Male",
//            "f" => "Female"
//        ]);
//        $grid->birth_day('Birth day')->editable('date');
//        $grid->phone('Phone')->editable();
//        $grid->address('Address')->editable();
        //$grid->avatar('Avatar')->image();
        $class_student = ClassStudents::all()->pluck('title','id');
        $grid->class_student_id('Class Student Name')->editable('select', $class_student);
        //$grid->description('Description')->editable('textarea');
        $grid->status('Status')->editable('select', [
            0 => "Dropped out of school",
            1 => "Studying",
            2 => "Graduated",
            3 => "Reserve"
        ]);
        $course = Courses::all()->pluck('title','id');
        $grid->course_id('Course Name')->editable('select', $course);
        $subject = Subjects::all()->pluck('title','id');
        $grid->subject_id('Subject Name')->editable('select', $subject);
        $pointTime = PointsTime::all()->pluck('title','id');
        $grid->point_time_id('Point Time')->editable('select', $pointTime);
        $grid->score('Score')->editable()->sortable();
//        $grid->deleted_at('Deleted at');
//        $grid->created_at('Created at');
//        $grid->updated_at('Updated at');

        $grid->filter(function ($filter){
            $filter->expand();

            $filter->column(1/2, function ($filter){
                $filter->equal('student_code','Student Code');
                $filter->like('full_name','Full Name');
                $filter->in('gender', 'Gender')->radio([
                    "m" => "Male",
                    "f" => "Female"
                ]);
                $filter->date('birth_day', 'Birthday');
                $filter->equal('phone', 'Phone');
                $filter->in('status', 'Status')->radio([
                    0 => "Dropped out of school",
                    1 => "Studying",
                    2 => "Graduated",
                    3 => "Reserve"
                ]);
           }) ;

            $filter->column(1/2, function ($filter){
                $course = Courses::all()->pluck('title','id');
                $filter->equal('course_id','Course Name')->select($course);
                $class_student = ClassStudents::all()->pluck('title','id');
                $filter->equal('class_student_id','Class Student Name')->select($class_student);
                $subject = Subjects::all()->pluck('title','id');
                $pointTime = PointsTime::all()->pluck('title','id');
                $filter->equal('subject_id','Subject Name')->select($subject);
                $filter->equal('pointTime','Point Time')->select($pointTime);
                $filter->equal('score','Score');
//                $filter->between('created_at','Created_at');
//                $filter->between('updated_at','Updated_at');
            });
        });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Students::findOrFail($id));

        $show->id('ID');
        $show->student_code('Student Code');
        $show->full_name('Full Name');
        $show->alias('Alias')->as(function ($alias){
            return str_slug($this->full_name);
        });
        $show->gender('Gender')->as(function ($gender){
            if($gender == "m")
            {
                return "Male";
            }
            else
            {
                return "Female";
            }
        });
        $show->birth_day('Birthday');
        $show->phone('Phone');
        $show->address('Address');
        $show->avatar('Avatar')->image();
        $show->class_student_id('Class student Name')->as(function ($id){
            $classSt = DB::table('class_students')->select('title')
                ->where('status', 1)->where('id', $id)->first();
            return $classSt->title;
        });
        $show->description('Description');
        $show->status('Status')->as(function ($status){
            if($status == 0)
            {
                return "Dropped out of school";
            }
            elseif ($status == 1)
            {
                return "Studying";
            }
            elseif ($status == 2)
            {
                return "Graduated";
            }
            else
            {
                return "Reserve";
            }
        })->badge();
        $show->course_id('Course Name')->as(function ($id){
            $course = DB::table('courses')->select('title')
                ->where('status', 1)->where('id', $id)->first();
            return $course->title;
        });
        $show->subject_id('Subject Name')->as(function ($id){
            $subject = DB::table('subjects')->select('title')
                ->where('status', 1)->where('id', $id)->first();
            return $subject->title;
        });
        $show->point_time_id('Point Time')->as(function ($id){
            $pointTime = DB::table('points_time')->select('title')
                ->where('status', 1)->where('id', $id)->first();
            return $pointTime->title;
        });
        $show->score('Score');
        $show->deleted_at('Deleted at');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Students);

        $form->text('student_code', 'Student Code')->rules('required|unique:students,student_code',[
            'required' => 'You have not entered information',
            'unique'   => 'Data cannot be duplicated'
        ]);
        $form->text('full_name', 'Full Name')->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $form->select('gender','Gender')->options(['f' => 'Female', 'm' => 'Male'])->default('m');
        $form->date('birth_day', 'Birth day')->default(date('Y-m-d'))->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $form->mobile('phone', 'Phone')->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $form->textarea('address', 'Address')->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $form->image('avatar','Avatar')->removable()->move('students')->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $form->select('class_student_id', 'Class Student Name')->options(ClassStudents::all()->pluck('title','id'))->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $form->textarea('description', 'Description');
        $form->select('course_id','Course Name')->options(Courses::all()->pluck('title','id'))->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $form->hidden('subject_id','Subject Name')->options(Subjects::all()->pluck('title','id'))->help( 'You can fill it out or later');
        $form->hidden('point_time_id','Point Time')->options(PointsTime::all()->pluck('title','id'))->help('You can fill it out or later');
        $form->hidden('score')->help('You can fill it out or later');
        $status = [
            0 => "Dropped out of school",
            1 => "Studying",
            2 => "Graduated",
            3 => "Reserve"
        ];
        $form->select('status', 'Status')->options($status)->default(1);

        return $form;
    }

    protected function form_edit()
    {
        $form = new Form(new Students);

        $form->text('student_code', 'Student Code')->rules('required',[
            'required' => 'You have not entered information'
        ])->disable();
        $form->text('full_name', 'Full Name')->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $form->select('gender','Gender')->options(['f' => 'Female', 'm' => 'Male'])->default('m');
        $form->date('birth_day', 'Birth day')->default(date('Y-m-d'))->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $form->mobile('phone', 'Phone')->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $form->textarea('address', 'Address')->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $form->image('avatar','Avatar')->removable()->move('students')->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $form->select('class_student_id', 'Class student id')->options(ClassStudents::all()->pluck('title','id'))->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $form->textarea('description', 'Description');
        $status = [
            0 => "Dropped out of school",
            1 => "Studying",
            2 => "Graduated",
            3 => "Reserve"
        ];
        $form->select('course_id','Course Name')->options(Courses::all()->pluck('title','id'));
        $form->select('subject_id','Subject Name')->options(Subjects::all()->pluck('title','id'));
        $form->select('point_time_id','Point Time')->options(PointsTime::all()->pluck('title','id'));
        $form->number('score');
        $form->select('status', 'Status')->options($status);

        return $form;
    }
}
