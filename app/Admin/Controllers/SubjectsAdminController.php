<?php

namespace App\Admin\Controllers;

use App\Subjects;
use App\Http\Controllers\Controller;
use App\Teachers;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Support\Facades\DB;

class SubjectsAdminController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Subjects);

        $grid->id('ID')->sortable();
        $grid->title('Title')->editable();
//        $grid->column('alias','Alias')->display(function (){
//            return str_slug($this->title);
//        });
        $grid->teaching_unit('Teaching Unit')->editable()->sortable();
        $teacher = Teachers::all()->pluck('full_name','id');
        $grid->teacher_id('Teacher Name')->editable('select', $teacher);
        $grid->description('Description')->editable();
        $grid->status('Status')->editable('select', [
            0 => "Disable",
            1 => "Enable"
        ]);
        $grid->deleted_at('Deleted at');
        $grid->created_at('Created at');
        $grid->updated_at('Updated at');

        $grid->filter(function ($filter){
            $filter->expand();

            $filter->column(1/2, function ($filter){
                $filter->like('title', 'Title');
                $filter->equal('teaching_unit','Teaching Unit');
                $teacher = Teachers::all()->pluck('full_name','id');
                $filter->equal('teacher_id', 'Teacher Name')->select($teacher);
            });

            $filter->column(1/2, function ($filter){
                $filter->in('status', 'Status')->radio([
                    0 => "Disable",
                    1 => "Enable"
                ]);
                $filter->between('created_at','Created_at');
                $filter->between('updated_at','Updated_at');
            });
        });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Subjects::findOrFail($id));

        $show->id('ID');
        $show->title('Title');
        $show->alias('Alias')->as(function (){
            return str_slug($this->title);
        });
        $show->teaching_unit('Teaching Unit');
        $show->teacher_id('Teacher Name')->as(function ($id){
            $teaching = DB::table('teachers')->select('full_name')
                ->where('id', $id)->first();
            return $teaching->full_name;
        });
        $show->description('Description');
        $show->status('Status')->as(function ($status){
            if($status == 0)
            {
                return "Disable";
            }
            else
            {
                return "Enable";
            }
        });
        $show->deleted_at('Deleted at');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Subjects);

        $form->text('title', 'Title')->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $form->number('teaching_unit', 'Teaching unit')->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $teacher = Teachers::all()->pluck('full_name', 'id');
        $form->select('teacher_id', 'Teacher Name')->options($teacher);
        $form->textarea('description', 'Description');
        $form->switch('status', 'Status')->default(1);

        return $form;
    }
}
