<?php

namespace App\Admin\Controllers;

use App\Facultys;
use App\Teachers;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class TeachersAdminController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form_edit()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Teachers);

        $grid->id('ID')->sortable();
        $grid->teacher_code('Teacher Code');
        $grid->full_name('Full Name')->editable();
//        $grid->column('alias','Alias')->display(function (){
//            return str_slug($this->full_name);
//        });
        $grid->gender('Gender')->editable('select', [
            "m" => "Male",
            "f" => "Female"
        ]);
        $grid->birth_day('Birthday')->editable('date');
        $grid->phone('Phone')->editable();
        $grid->address('Address')->editable();
        $grid->avatar('Avatar')->image();
        $faculty = Facultys::all()->pluck('title', 'id');
        $grid->faculty_id('Faculty Name')->editable('select', $faculty);
        $grid->description('Description')->editable('textarea');
        $grid->status('Status')->editable('select', [
            0 => "Stopped teaching",
            1 => "Teaching",
            2 => "Vacation Plan"
        ]);
        $grid->deleted_at('Deleted at');
        $grid->created_at('Created at');
        $grid->updated_at('Updated at');

        $grid->filter(function ($filter){
            $filter->expand();

            $filter->column(1/2, function ($filter){
                $filter->equal('teacher_code','Teacher Code');
                $filter->like('full_name','Full Name');
                $filter->in('gender', 'Gender')->radio([
                    "m" => "Male",
                    "f" => "Female"
                ]);
                $filter->date('birth_day', 'Birthday');
                $filter->equal('phone', 'Phone');
            }) ;

            $filter->column(1/2, function ($filter){
                $faculty = Facultys::all()->pluck('title','id');
                $filter->equal('faculty_id','Faculty Name')->select($faculty);
                $filter->in('status', 'Status')->radio([
                    0 => "Stopped teaching",
                    1 => "Teaching",
                    2 => "Vacation Plan"
                ]);
                $filter->between('created_at','Created_at');
                $filter->between('updated_at','Updated_at');
            });
        });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Teachers::findOrFail($id));

        $show->id('Id');
        $show->teacher_code('Teacher Code');
        $show->full_name('Full Name');
        $show->alias('Alias')->as(function ($alias){
            return str_slug($this->full_name);
        });
        $show->gender('Gender')->as(function ($gender){
            if($gender == "m")
            {
                return "Male";
            }
            else
            {
                return "Female";
            }
        });
        $show->birth_day('Birth day');
        $show->phone('Phone');
        $show->address('Address');
        $show->avatar('Avatar')->image();
        $show->faculty_id('Faculty Name')->as(function ($id){
            $fac = DB::table('facultys')->select('title')
                ->where('status', 1)->where('id', $id)->first();
            return $fac->title;
        });
        $show->description('Description');
        $show->status('Status')->as(function ($status){
            if($status == 0)
            {
                return "Stopped teaching";
            }
            elseif ($status == 1)
            {
                return "Teaching";
            }
            else
            {
                return "Vacation Plan";
            }
        })->badge();
        $show->deleted_at('Deleted at');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Teachers);

        $form->text('teacher_code', 'Teacher Code')->rules('required|unique:teachers,teacher_code',[
            'required' => 'You have not entered information',
            'unique'   => 'Data cannot be duplicated'
        ]);
        $form->text('full_name', 'Full Name')->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $form->select('gender','Gender')->options(['f' => 'Female', 'm' => 'Male'])->default('m');
        $form->date('birth_day', 'Birth day')->default(date('Y-m-d'))->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $form->mobile('phone', 'Phone')->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $form->textarea('address', 'Address')->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $form->image('avatar','Avatar')->removable()->move('teachers')->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $form->select('faculty_id', 'Faculty Name')->options(Facultys::all()->pluck('title','id'))->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $form->textarea('description', 'Description');
        $status = [
            0 => "Stopped teaching",
            1 => "Teaching",
            2 => "Vacation Plan"
        ];
        $form->select('status', 'Status')->options($status)->default(1);

        return $form;
    }

    protected function form_edit()
    {
        $form = new Form(new Teachers);

        $form->text('teacher_code', 'Teacher Code')->rules('required',[
            'required' => 'You have not entered information'
        ])->disable();
        $form->text('full_name', 'Full Name')->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $form->select('gender','Gender')->options(['f' => 'Female', 'm' => 'Male'])->default('m');
        $form->date('birth_day', 'Birth day')->default(date('Y-m-d'))->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $form->mobile('phone', 'Phone')->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $form->textarea('address', 'Address')->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $form->image('avatar','Avatar')->removable()->move('teachers')->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $form->select('faculty_id', 'Faculty Name')->options(Facultys::all()->pluck('title','id'))->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $form->textarea('description', 'Description');
        $status = [
            0 => "Stopped teaching",
            1 => "Teaching",
            2 => "Vacation Plan"
        ];
        $form->select('status', 'Status')->options($status);

        return $form;
    }
}
