<?php

namespace App\Admin\Controllers;

use App\ClassStudents;
use App\Facultys;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Support\Facades\DB;

class ClassStudentsAdminController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ClassStudents);

        $grid->id('ID')->sortable();
        $grid->title('Title')->editable();
        $facultys = Facultys::all()->pluck('title','id');
        $grid->faculty_id('Faculty Name')->editable('select', $facultys);
//        $grid->column('alias', 'Alias')->display(function ($alias){
//            return str_slug($this->title);
//        });
        $grid->description('Description')->editable('textarea');
        $grid->status('Status')->editable('select', [
            0 => "Disable",
            1 => "Enable"
        ]);
        $grid->deleted_at('Deleted at');
        $grid->created_at('Created at');
        $grid->updated_at('Updated at');

        $grid->filter(function ($filter){
            $filter->expand();

            $filter->column(1/2, function ($filter){
               $filter->like('title', 'Title');
               $faculty = Facultys::all()->pluck('title','id');
               $filter->equal('faculty_id', 'Faculty Name')->select($faculty);
            });

            $filter->column(1/2, function ($filter){
                $filter->in('status', 'Status')->radio([
                    0 => "Disable",
                    1 => "Enable"
                ]);
                $filter->between('created_at','Created_at');
                $filter->between('updated_at','Updated_at');
            });
        });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ClassStudents::findOrFail($id));

        $show->id('ID');
        $show->title('Title');
        $show->faculty_id('Faculty Name')->as(function ($id){
            $fa = DB::table('facultys')->select('title')
                ->where('id', $id)->where('status', 1)->first();
            return $fa->title;
        });
        $show->alias('Alias')->as(function (){
            return str_slug($this->title);
        });
        $show->description('Description');
        $show->status('Status')->as(function ($status){
            if($status == 0)
            {
                return "Disable";
            }
            else
            {
                return "Enable";
            }
        });
        $show->deleted_at('Deleted at');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ClassStudents);

        $form->text('title', 'Title')->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $form->select('faculty_id', 'Faculty Name')->options(Facultys::all()->pluck('title','id'))->rules('required',[
            'required' => 'You have not entered information'
        ])->help('Please select facultys ....');
        $form->textarea('description', 'Description');
        $form->switch('status', 'Status')->default(1)->help('Value default is Enable');

        return $form;
    }
}
