<?php

namespace App\Admin\Controllers;

use App\PointsType;
use App\PointsTime;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Support\Facades\DB;

class PointsTimeAdminController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new PointsTime);

        $grid->id('ID')->sortable();
        $grid->title('Title')->editable();
//        $grid->column('alias', 'Alias')->display(function(){
//            return str_slug($this->title);
//        });
        $point = PointsType::all()->pluck('title','id');
        $grid->point_type_id('Point Type')->editable('select',$point);
        $grid->description('Description')->editable();
        $grid->status('Status')->editable('select', [
            0 => "Disable",
            1 => "Enable"
        ]);
        $grid->deleted_at('Deleted at');
        $grid->created_at('Created at');
        $grid->updated_at('Updated at');

        $grid->filter(function ($filter){
            $filter->expand();

            $filter->column(1/2, function ($filter){
                $filter->like('title', 'Title');
                $point = PointsType::all()->pluck('title','id');
                $filter->equal('point_type_id','Point Name')->select($point);
            });

            $filter->column(1/2, function ($filter){
                $filter->in('status', 'Status')->radio([
                    0 => "Disable",
                    1 => "Enable"
                ]);
                $filter->between('created_at')->datetime();
                $filter->between('updated_at')->datetime();
            });

        });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(PointsTime::findOrFail($id));

        $show->id('ID');
        $show->title('Title');
        $show->alias('Alias')->as(function ($alias){
            return str_slug($this->title);
        });
        $show->point_type_id('Point Type')->as(function ($id){
            $point = DB::table('points')->select('title')
                ->where('status', 1)->where('id', $id)->first();
            return $point->title;
        });
        $show->description('Description');
        $show->status('Status')->as(function ($status){
            if($status == 0)
            {
                return "Disable";
            }
            else
            {
                return "Enable";
            }
        });
        $show->deleted_at('Deleted at');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new PointsTime);

        $form->text('title', 'Title')->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $point = PointsType::all()->pluck('title','id');
        $form->select('point_type_id', 'Point Name')->options($point);
        $form->textarea('description', 'Description');
        $form->switch('status', 'Status')->default(1)->help('Value default is Enable');

        return $form;
    }
}
