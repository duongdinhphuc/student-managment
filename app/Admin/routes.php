<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index');
    $router->resource('/facultys', FacultysAdminController::class);
    $router->resource('/courses', CoursesAdminController::class);
    $router->resource('/classstudents', ClassStudentsAdminController::class);
    $router->resource('/pointstype', PointsTypeAdminController::class);
    $router->resource('/pointstime', PointsTimeAdminController::class);
    $router->resource('/pointsdetail', PointsDetailAdminController::class);
    $router->resource('/students', StudentsAdminController::class);
    $router->resource('/teachers', TeachersAdminController::class);
    $router->resource('/subjects', SubjectsAdminController::class);
    $router->resource('/users', UsersAdminController::class);

});
