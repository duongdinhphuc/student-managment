<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStudents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('student_code', 100)->nullable();
            $table->string('full_name', 100)->nullable();
            $table->string('alias', 100)->nullable();
            $table->date('birth_day')->nullable();
            $table->string('phone', 100)->nullable();
            $table->text('address')->nullable();
            $table->integer('class_student_id')->unsigned()->nullable();
            $table->text('description')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->index(['student_code']);
            $table->index(['full_name']);
            $table->index(['birth_day']);
            $table->index(['phone']);
            //$table->index(['address']);
            $table->index(['class_student_id']);
            $table->index(['status']);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
