<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSubjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title', 100)->nullable();
            $table->string('alias', 100)->nullable();
            $table->integer('teaching_unit')->nullable();
            $table->integer('teacher_id')->unsigned()->nullable();
            $table->text('description')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->index(['title']);
            $table->index(['teaching_unit']);
            $table->index(['teacher_id']);
            $table->index(['status']);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subjects');
    }
}
